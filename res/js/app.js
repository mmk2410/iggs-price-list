// 2018 (c) Marcel Kapfer
// MIT License

const app = new Vue({
    el: '#app',
    data: {
        snacks: [],
        search: "",
        count: "Die Snacks werden geladen..."
    },
    methods: {
        matches (snack) {
            let searchStr = this.search.replace(/ /gi, '.*');
            return snack.name.toLowerCase().search(searchStr.toLowerCase()) !== -1;
        }
    },
    created () {
        fetch('prices.json')
            .then(response => response.json())
            .then(json => {
                this.snacks = json;
            });
    },
});
